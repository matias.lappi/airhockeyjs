import { Player } from "../classes/player.js";
function getDistanceBetweenTwoPoints(pointOne, pointTwo) {
    return Math.sqrt(Math.pow((pointTwo.x - pointOne.x), 2) + Math.pow((pointTwo.y - pointOne.y), 2));
}
function getDistanceBetweenTwoObjects(objectOne, objectTwo) {
    const distanceBetweenCenters = getDistanceBetweenTwoPoints(objectOne.position, objectTwo.position);
    const distanceBetweenObjects = distanceBetweenCenters - (objectOne.radius + objectTwo.radius);
    return distanceBetweenObjects;
}
function getAngleBetweenTwoPoints(pointOne, pointTwo) {
    return Math.atan2(pointTwo.y - pointOne.y, pointTwo.x - pointOne.x);
}
function getVectorMagnitude(vector) {
    return Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
}
function getVectorAngle(vector) {
    return Math.atan2(vector.y, vector.x);
}
function getVectorComponentAlong(vector, component, targetAngle) {
    const magnitude = getVectorMagnitude(vector);
    const angle = getVectorAngle(vector);
    const angleDifference = angle - targetAngle;
    if (component === 'x') {
        return magnitude * Math.cos(angleDifference);
    }
    else if (component === 'y') {
        return magnitude * Math.sin(angleDifference);
    }
    else {
        throw new Error("Invalid component. Must be 'x' or 'y'.");
    }
}
function calculateElasticCollision(m1, v1, m2, v2) {
    return (2 * m2 * v2 + v1 * (m1 - m2)) / (m1 + m2);
}
function calculateCollision(puck, otherObject, angle, puckVelocityX, otherObjectVelocityX) {
    const puckFinalVelocityX = calculateElasticCollision(puck.mass, puckVelocityX, otherObject.mass, otherObjectVelocityX);
    const puckFinalVelocityY = getVectorComponentAlong(puck.velocity, 'y', angle);
    puck.velocity.x = Math.cos(angle) * puckFinalVelocityX + Math.cos(angle + Math.PI / 2) * puckFinalVelocityY;
    puck.velocity.y = Math.sin(angle) * puckFinalVelocityX + Math.sin(angle + Math.PI / 2) * puckFinalVelocityY;
}
export function handleObjectCollision(objectOne, objectTwo) {
    if (objectOne instanceof Player && objectTwo instanceof Player)
        return;
    const distance = getDistanceBetweenTwoObjects(objectOne, objectTwo);
    if (distance > 0)
        return;
    const angle = getAngleBetweenTwoPoints(objectOne.position, objectTwo.position);
    const objectOneVelocityX = getVectorComponentAlong(objectOne.velocity, 'x', angle);
    const objectTwoVelocityX = getVectorComponentAlong(objectTwo.velocity, 'x', angle);
    if (!(objectOne instanceof Player)) {
        calculateCollision(objectOne, objectTwo, angle, objectOneVelocityX, objectTwoVelocityX);
    }
    if (!(objectTwo instanceof Player)) {
        calculateCollision(objectTwo, objectOne, angle, objectTwoVelocityX, objectOneVelocityX);
    }
    const overlap = -distance;
    if (overlap > 0) {
        objectOne.position.x -= overlap / 2 * Math.cos(angle);
        objectOne.position.y -= overlap / 2 * Math.sin(angle);
        objectTwo.position.x += overlap / 2 * Math.cos(angle);
        objectTwo.position.y += overlap / 2 * Math.sin(angle);
    }
}
export function getRandomNumberBetween(min, max) {
    return Math.random() * (max - min) + min;
}
