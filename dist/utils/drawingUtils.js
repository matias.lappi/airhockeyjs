export function drawRectangle(ctx, position, width, height, color) {
    ctx.fillStyle = color;
    ctx.fillRect(position.x - width / 2, position.y - height / 2, width, height);
}
export function drawCircle(ctx, position, radius, color, lineWidth) {
    ctx.beginPath();
    if (lineWidth) {
        ctx.arc(position.x, position.y, radius - lineWidth / 2, 0, Math.PI * 2, true);
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;
        ctx.stroke();
    }
    else {
        ctx.arc(position.x, position.y, radius, 0, Math.PI * 2, true);
        ctx.fillStyle = color;
        ctx.fill();
    }
}
