import { PhysicsBody } from "./physicsBody.js";
import { drawCircle } from "../utils/drawingUtils.js";
export class DrawableObject extends PhysicsBody {
    constructor(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox, primaryColor, accentColor) {
        super(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox);
        this.primaryColor = primaryColor;
        this.accentColor = accentColor;
    }
    drawObject(ctx) {
        const accentCircleRadius = this.radius / 5 * 4;
        const accentCircleWidth = this.radius / 8;
        drawCircle(ctx, this.position, this.radius, this.primaryColor);
        drawCircle(ctx, this.position, accentCircleRadius, this.accentColor, accentCircleWidth);
    }
}
