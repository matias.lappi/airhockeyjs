import { DrawableObject } from "./drawableObject.js";
export class Player extends DrawableObject {
    constructor(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox, primaryColor, accentColor, controller, name, goal, score) {
        super(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox, primaryColor, accentColor);
        this.controller = controller;
        this.name = name;
        this.goal = goal;
        this.score = score;
        this.controller.addEventListeners(this.velocity);
        this.updateScoreUI();
    }
    updateScoreUI() {
        const scoreUIElement = document.getElementById(`${this.name}-score`);
        if (scoreUIElement) {
            scoreUIElement.innerText = String(this.score);
        }
    }
    incrementScore() {
        this.score = ++this.score;
        this.updateScoreUI();
    }
    resetScore() {
        this.score = 0;
        this.updateScoreUI();
    }
    updateObject(ctx) {
        this.updatePosition();
        this.movementBox.handleMovementBoxCollision(this);
        this.drawObject(ctx);
    }
}
