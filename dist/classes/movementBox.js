import { Box } from "./box.js";
export class MovementBox extends Box {
    constructor(location, width, height) {
        super(location, width, height);
    }
    handleMovementBoxCollision(body) {
        if (!this.isInsideLeftLimit(body)) {
            body.position.x = this.leftLimit + body.radius;
            body.velocity.x *= -body.collisionCoefficient;
        }
        if (!this.isInsideRightLimit(body)) {
            body.position.x = this.rightLimit - body.radius;
            body.velocity.x *= -body.collisionCoefficient;
        }
        if (!this.isInsideTopLimit(body)) {
            body.position.y = this.topLimit + body.radius;
            body.velocity.y *= -body.collisionCoefficient;
        }
        if (!this.isInsideBottomLimit(body)) {
            body.position.y = this.bottomLimit - body.radius;
            body.velocity.y *= -body.collisionCoefficient;
        }
    }
}
