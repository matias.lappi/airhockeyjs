import { DrawableObject } from "./drawableObject.js";
export class Puck extends DrawableObject {
    constructor(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox, primaryColor, accentColor) {
        super(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox, primaryColor, accentColor);
    }
    updateObject(ctx) {
        this.updatePosition();
        this.movementBox.handleMovementBoxCollision(this);
        this.drawObject(ctx);
        this.applyFriction();
    }
}
