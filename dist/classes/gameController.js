// Program name: AirHockeyJS
// Author: Matias Lappi
// This code is my original work
import { GameBoard } from './gameBoard.js';
import { Goal } from './goal.js';
import { Player } from './player.js';
import { Puck } from './puck.js';
import { handleObjectCollision } from '../utils/mathUtils.js';
import { MovementBox } from './movementBox.js';
import { Controller } from './controller.js';
class GameController {
    constructor() {
        this.gameFPS = 60;
        this.amountOfPucks = 1;
        this.pucks = [];
        this.players = [];
        this.gameObjects = [];
        this.gameBoard = this.initGameBoard();
        this.initGameObjects();
        this.addEventListeners();
        this.initGame();
    }
    initGameBoard() {
        const width = 860;
        const height = 580;
        const centerX = width / 2;
        const centerY = height / 2;
        const lineOpacity = 0.4;
        const gameBoardConfig = {
            canvasId: 'canvas',
            width: width,
            height: height,
            center: { x: centerX, y: centerY },
            lineWidth: 6,
            dotRadius: 6,
            goalPositions: [
                { x: 0, y: centerY },
                { x: width, y: centerY }
            ],
            offSideLinePositions: [
                { x: width / 3, y: centerY },
                { x: width / 3 * 2, y: centerY }
            ],
            faceOffCirclePositions: [
                { x: width / 6, y: height / 4 },
                { x: width / 6, y: height / 4 * 3 },
                { x: width / 6 * 5, y: height / 4 },
                { x: width / 6 * 5, y: height / 4 * 3 }
            ],
            circleRadiusSmall: 50,
            circleRadiusLarge: 80,
            circleRadiusGoal: 80,
            iceColor: '#f6f6f6',
            redColor: `rgba(255, 0, 0, ${lineOpacity})`,
            blueColor: `rgba(0, 0, 255, ${lineOpacity})`
        };
        return new GameBoard(gameBoardConfig.canvasId, gameBoardConfig.width, gameBoardConfig.height, gameBoardConfig.center, gameBoardConfig.lineWidth, gameBoardConfig.dotRadius, gameBoardConfig.goalPositions, gameBoardConfig.offSideLinePositions, gameBoardConfig.faceOffCirclePositions, gameBoardConfig.circleRadiusSmall, gameBoardConfig.circleRadiusLarge, gameBoardConfig.circleRadiusGoal, gameBoardConfig.iceColor, gameBoardConfig.redColor, gameBoardConfig.blueColor);
    }
    initPucks() {
        const commonPuckConfig = {
            mass: 1,
            radius: 30,
            startVelocity: { x: 0, y: 0 },
            collisionCoefficient: 0.8,
            frictionCoefficient: 0.996,
            movementBox: new MovementBox(this.gameBoard.center, this.gameBoard.width, this.gameBoard.height),
            primaryColor: '#000000',
            accentColor: '#aaaaaa'
        };
        const puckConfigs = [{
                startPosition: {
                    x: this.gameBoard.center.x,
                    y: this.gameBoard.center.y
                }
            }];
        const puckSeparation = commonPuckConfig.radius * 6 / 5;
        if (this.amountOfPucks === 2) {
            puckConfigs[0].startPosition.y = this.gameBoard.center.y - puckSeparation;
            puckConfigs.push({
                startPosition: {
                    x: this.gameBoard.center.x,
                    y: this.gameBoard.center.y + puckSeparation
                }
            });
        }
        else if (this.amountOfPucks === 4) {
            puckConfigs[0].startPosition.x = this.gameBoard.center.x - puckSeparation;
            puckConfigs[0].startPosition.y = this.gameBoard.center.y - puckSeparation;
            puckConfigs.push({
                startPosition: {
                    x: this.gameBoard.center.x + puckSeparation,
                    y: this.gameBoard.center.y - puckSeparation
                }
            }, {
                startPosition: {
                    x: this.gameBoard.center.x - puckSeparation,
                    y: this.gameBoard.center.y + puckSeparation
                }
            }, {
                startPosition: {
                    x: this.gameBoard.center.x + puckSeparation,
                    y: this.gameBoard.center.y + puckSeparation
                }
            });
        }
        return puckConfigs.map(puckConfig => new Puck(commonPuckConfig.mass, commonPuckConfig.radius, puckConfig.startPosition, commonPuckConfig.startVelocity, commonPuckConfig.collisionCoefficient, commonPuckConfig.frictionCoefficient, commonPuckConfig.movementBox, commonPuckConfig.primaryColor, commonPuckConfig.accentColor));
    }
    initPlayers() {
        const commonPlayerConfig = {
            mass: 6,
            radius: 40,
            startVelocity: { x: 0, y: 0 },
            collisionCoefficient: 0,
            frictionCoefficient: 1,
            score: 0
        };
        const targetSpeed = 8;
        const playerConfigs = [{
                startPosition: {
                    x: this.gameBoard.width / 5,
                    y: this.gameBoard.center.y
                },
                movementBox: new MovementBox({ x: this.gameBoard.center.x / 2, y: this.gameBoard.center.y }, this.gameBoard.width / 2, this.gameBoard.height),
                primaryColor: '#0000ff',
                accentColor: '#8888ff',
                controller: new Controller(targetSpeed, 'a', 'd', 'w', 's'),
                name: 'player-one',
                goal: new Goal({ x: this.gameBoard.width - this.pucks[0].radius, y: this.gameBoard.center.y }, this.pucks[0].diameter, this.gameBoard.circleRadiusGoal * 2)
            },
            {
                startPosition: {
                    x: this.gameBoard.width / 5 * 4,
                    y: this.gameBoard.center.y
                },
                movementBox: new MovementBox({ x: this.gameBoard.center.x / 2 * 3, y: this.gameBoard.center.y }, this.gameBoard.width / 2, this.gameBoard.height),
                primaryColor: '#ff0000',
                accentColor: '#ff8888',
                controller: new Controller(targetSpeed, 'ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'),
                name: 'player-two',
                goal: new Goal({ x: this.pucks[0].radius, y: this.gameBoard.center.y }, this.pucks[0].diameter, this.gameBoard.circleRadiusGoal * 2)
            }];
        return playerConfigs.map(playerConfig => new Player(commonPlayerConfig.mass, commonPlayerConfig.radius, playerConfig.startPosition, commonPlayerConfig.startVelocity, commonPlayerConfig.collisionCoefficient, commonPlayerConfig.frictionCoefficient, playerConfig.movementBox, playerConfig.primaryColor, playerConfig.accentColor, playerConfig.controller, playerConfig.name, playerConfig.goal, commonPlayerConfig.score));
    }
    initGameObjects() {
        this.pucks = this.initPucks();
        this.players = this.initPlayers();
        this.gameObjects = [...this.pucks, ...this.players];
    }
    resetPlayerScores() {
        for (const player of this.players) {
            player.resetScore();
        }
    }
    resetGameObjectPositions() {
        for (const gameObject of this.gameObjects) {
            gameObject.resetPosition();
        }
    }
    resetGame() {
        this.resetPlayerScores();
        this.resetGameObjectPositions();
    }
    initRound() {
        this.resetGameObjectPositions();
    }
    addResetGameEventListener() {
        const resetButton = document.getElementById('reset-button');
        resetButton === null || resetButton === void 0 ? void 0 : resetButton.addEventListener('click', () => this.resetGame(), false);
    }
    addPuckAmountEventListener() {
        const puckAmountDropDown = document.getElementById('puck-amount-dropdown');
        puckAmountDropDown === null || puckAmountDropDown === void 0 ? void 0 : puckAmountDropDown.addEventListener('change', () => {
            this.amountOfPucks = Number(puckAmountDropDown.value);
            this.initRound();
            this.initGameObjects();
            puckAmountDropDown.blur();
        }, false);
    }
    addEventListeners() {
        this.addResetGameEventListener();
        this.addPuckAmountEventListener();
    }
    updateGame() {
        const gameBoard = this.gameBoard;
        const pucks = this.pucks;
        const players = this.players;
        const gameObjects = this.gameObjects;
        gameBoard.drawBackground();
        for (let i = 0; i < gameObjects.length; i++) {
            for (let j = i + 1; j < gameObjects.length; j++) {
                handleObjectCollision(gameObjects[i], gameObjects[j]);
            }
            gameObjects[i].updateObject(gameBoard.ctx);
        }
        for (const player of players) {
            for (const puck of pucks) {
                if (player.goal.isPuckInsideGoal(puck)) {
                    player.incrementScore();
                    this.initRound();
                }
            }
        }
    }
    initGame() {
        setInterval(() => this.updateGame(), 1000 / this.gameFPS);
    }
}
new GameController();
