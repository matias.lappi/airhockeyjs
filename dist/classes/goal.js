import { Box } from "./box.js";
export class Goal extends Box {
    constructor(location, width, height) {
        super(location, width, height);
    }
    isPuckInsideGoal(puck) {
        const isWithinXBounds = this.isInsideLeftLimit(puck) && this.isInsideRightLimit(puck);
        const isWithinYBounds = this.isInsideTopLimit(puck) && this.isInsideBottomLimit(puck);
        return isWithinXBounds && isWithinYBounds;
    }
}
