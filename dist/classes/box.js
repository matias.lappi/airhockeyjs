export class Box {
    constructor(location, width, height) {
        this.location = location;
        this.width = width;
        this.height = height;
        this.leftLimit = this.location.x - this.width / 2;
        this.rightLimit = this.location.x + this.width / 2;
        this.topLimit = this.location.y - this.height / 2;
        this.bottomLimit = this.location.y + this.height / 2;
    }
    isInsideLeftLimit(body) {
        return body.position.x - body.radius >= this.leftLimit;
    }
    isInsideRightLimit(body) {
        return body.position.x + body.radius <= this.rightLimit;
    }
    isInsideTopLimit(body) {
        return body.position.y - body.radius >= this.topLimit;
    }
    isInsideBottomLimit(body) {
        return body.position.y + body.radius <= this.bottomLimit;
    }
}
