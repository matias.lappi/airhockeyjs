import { drawRectangle, drawCircle } from "../utils/drawingUtils.js";
export class GameBoard {
    constructor(canvasId, width, height, center, lineWidth, dotRadius, goalPositions, offSideLinePositions, faceOffCirclePositions, circleRadiusSmall, circleRadiusLarge, circleRadiusGoal, iceColor, redColor, blueColor) {
        this.canvas = document.getElementById(canvasId);
        this.ctx = this.canvas.getContext('2d');
        this.width = width;
        this.height = height;
        this.canvas.width = width;
        this.canvas.height = height;
        this.center = center;
        this.lineWidth = lineWidth;
        this.dotRadius = dotRadius;
        this.goalPositions = goalPositions;
        this.offSideLinePositions = offSideLinePositions;
        this.faceOffCirclePositions = faceOffCirclePositions;
        this.circleRadiusSmall = circleRadiusSmall;
        this.circleRadiusLarge = circleRadiusLarge;
        this.circleRadiusGoal = circleRadiusGoal;
        this.iceColor = iceColor;
        this.redColor = redColor;
        this.blueColor = blueColor;
    }
    drawBackground() {
        drawRectangle(this.ctx, this.center, this.width, this.height, this.iceColor);
        drawRectangle(this.ctx, this.center, this.lineWidth, this.height, this.redColor);
        drawCircle(this.ctx, this.center, this.circleRadiusLarge, this.blueColor, this.lineWidth);
        for (const goalPosition of this.goalPositions) {
            drawCircle(this.ctx, goalPosition, this.circleRadiusGoal, this.blueColor, this.lineWidth);
        }
        for (const offSideLinePosition of this.offSideLinePositions) {
            drawRectangle(this.ctx, offSideLinePosition, this.lineWidth, this.height, this.blueColor);
        }
        for (const faceOffCirclePosition of this.faceOffCirclePositions) {
            drawCircle(this.ctx, faceOffCirclePosition, this.circleRadiusSmall, this.redColor, this.lineWidth);
            drawCircle(this.ctx, faceOffCirclePosition, this.dotRadius, this.redColor);
        }
    }
}
