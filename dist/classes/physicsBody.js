import { getRandomNumberBetween } from "../utils/mathUtils.js";
export class PhysicsBody {
    constructor(mass, radius, startPosition, startVelocity, collisionCoefficient, frictionCoefficient, movementBox) {
        this.position = { x: 0, y: 0 };
        this.velocity = { x: 0, y: 0 };
        this.mass = mass;
        this.radius = radius;
        this.diameter = radius * 2;
        this.startPosition = startPosition;
        this.startVelocity = startVelocity;
        this.collisionCoefficient = collisionCoefficient;
        this.frictionCoefficient = frictionCoefficient;
        this.movementBox = movementBox;
        this.resetPosition();
    }
    updatePosition() {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }
    applyFriction() {
        this.velocity.x *= this.frictionCoefficient;
        this.velocity.y *= this.frictionCoefficient;
    }
    resetPosition() {
        this.position.x = this.startPosition.x + getRandomNumberBetween(-2, 2);
        this.position.y = this.startPosition.y + getRandomNumberBetween(-2, 2);
        this.velocity.x = this.startVelocity.x;
        this.velocity.y = this.startVelocity.y;
    }
}
