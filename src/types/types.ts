import { Puck } from "../classes/puck.js"
import { Player } from "../classes/player.js"
import { MovementBox } from "../classes/movementBox.js"
import { Controller } from "../classes/controller.js"
import { Goal } from "../classes/goal.js"

export interface Point {
    x: number;
    y: number;
}

export interface Vector {
    x: number;
    y: number;
}

export interface GameBoardConfig {
    canvasId: string;
    width: number;
    height: number;
    center: Point;
    lineWidth: number;
    dotRadius: number;
    goalPositions: Point[];
    offSideLinePositions: Point[];
    faceOffCirclePositions: Point[];
    circleRadiusSmall: number;
    circleRadiusLarge: number;
    circleRadiusGoal: number;
    iceColor: string;
    redColor: string;
    blueColor: string;
}

export interface CommonPuckConfig {
    mass: number;
    radius: number;
    startVelocity: Vector;
    collisionCoefficient: number;
    frictionCoefficient: number;
    movementBox: MovementBox;
    primaryColor: string;
    accentColor: string;
}

export interface PuckConfig {
    startPosition: Point;
}

export interface CommonPlayerConfig {
    mass: number;
    radius: number;
    startVelocity: Vector;
    collisionCoefficient: number;
    frictionCoefficient: number;
    score: number;
}

export interface PlayerConfig {
    startPosition: Point;
    movementBox: MovementBox;
    primaryColor: string;
    accentColor: string;
    controller: Controller;
    name: string;
    goal: Goal;
}

export type GameObject = Puck | Player;

export type NullableHTMLButtonElement = HTMLButtonElement | null;

export type NullableHTMLSelectElement = HTMLSelectElement | null;

export type NullableHTMLParagraphElement = HTMLParagraphElement | null;
