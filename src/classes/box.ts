import { Point } from "../types/types";
import { PhysicsBody } from "./physicsBody";

export abstract class Box {
    readonly location: Point;
    readonly width: number;
    readonly height: number;
    readonly leftLimit: number;
    readonly rightLimit: number;
    readonly topLimit: number;
    readonly bottomLimit: number

    constructor(location: Point, width: number, height: number) {
        this.location = location;
        this.width = width;
        this.height = height;
        this.leftLimit = this.location.x - this.width / 2;
        this.rightLimit = this.location.x + this.width / 2;
        this.topLimit = this.location.y - this.height / 2;
        this.bottomLimit = this.location.y + this.height / 2;
    }

    isInsideLeftLimit(body: PhysicsBody): boolean {
        return body.position.x - body.radius >= this.leftLimit;
    }

    isInsideRightLimit(body: PhysicsBody): boolean {
        return body.position.x + body.radius <= this.rightLimit;
    }

    isInsideTopLimit(body: PhysicsBody): boolean {
        return body.position.y - body.radius >= this.topLimit;
    }

    isInsideBottomLimit(body: PhysicsBody): boolean {
        return body.position.y + body.radius <= this.bottomLimit;
    }
}
