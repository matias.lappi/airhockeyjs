import { DrawableObject } from "./drawableObject.js";
import { Point, Vector } from "../types/types.js";
import { MovementBox } from "./movementBox.js";

export class Puck extends DrawableObject {
    constructor(
        mass: number, radius: number, startPosition: Point, startVelocity: Vector,
        collisionCoefficient: number, frictionCoefficient: number, movementBox: MovementBox,
        primaryColor: string, accentColor: string
    ) {
        super(
            mass, radius, startPosition, startVelocity,
            collisionCoefficient, frictionCoefficient, movementBox,
            primaryColor, accentColor
        );
    }

    updateObject(ctx: CanvasRenderingContext2D): void {
        this.updatePosition();
        this.movementBox.handleMovementBoxCollision(this);
        this.drawObject(ctx);
        this.applyFriction();
    }
}
