import { DrawableObject } from "./drawableObject.js";
import { MovementBox } from "./movementBox.js";
import { Goal } from "./goal.js";
import { Controller } from "./controller.js";
import { NullableHTMLParagraphElement, Point, Vector } from "../types/types.js";

export class Player extends DrawableObject {
    readonly controller: Controller;
    readonly name: string;
    readonly goal: Goal;
    score: number;

    constructor(
        mass: number, radius: number, startPosition: Point, startVelocity: Vector,
        collisionCoefficient: number, frictionCoefficient: number, movementBox: MovementBox,
        primaryColor: string, accentColor: string, controller: Controller,
        name: string, goal: Goal, score: number
    ) {
        super(
            mass, radius, startPosition, startVelocity,
            collisionCoefficient, frictionCoefficient, movementBox,
            primaryColor, accentColor
        );
        this.controller = controller;
        this.name = name;
        this.goal = goal;
        this.score = score;
        this.controller.addEventListeners(this.velocity);
        this.updateScoreUI();
    }

    private updateScoreUI(): void {
        const scoreUIElement = document.getElementById(`${this.name}-score`) as NullableHTMLParagraphElement;
        if (scoreUIElement) {
            scoreUIElement.innerText = String(this.score);
        }
    }

    incrementScore(): void {
        this.score = ++this.score;
        this.updateScoreUI();
    }

    resetScore(): void {
        this.score = 0;
        this.updateScoreUI();
    }

    updateObject(ctx: CanvasRenderingContext2D): void {
        this.updatePosition();
        this.movementBox.handleMovementBoxCollision(this);
        this.drawObject(ctx);
    }
}
