import { MovementBox } from "./movementBox.js";
import { Point, Vector } from "../types/types.js";
import { getRandomNumberBetween } from "../utils/mathUtils.js";

export abstract class PhysicsBody {
    readonly mass: number;
    readonly radius: number;
    readonly diameter: number;
    readonly startPosition: Point;
    position: Point = { x: 0, y: 0 };
    readonly startVelocity: Vector;
    velocity: Vector = { x: 0, y: 0};
    readonly collisionCoefficient: number;
    readonly frictionCoefficient: number;
    readonly movementBox: MovementBox;

    constructor(
        mass: number, radius: number, startPosition: Point, startVelocity: Vector,
        collisionCoefficient: number, frictionCoefficient: number, movementBox: MovementBox
    ) {
        this.mass = mass;
        this.radius = radius;
        this.diameter = radius * 2;
        this.startPosition = startPosition;
        this.startVelocity = startVelocity;
        this.collisionCoefficient = collisionCoefficient;
        this.frictionCoefficient = frictionCoefficient;
        this.movementBox = movementBox;
        this.resetPosition();
    }

    updatePosition(): void {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }

    applyFriction(): void {
        this.velocity.x *= this.frictionCoefficient;
        this.velocity.y *= this.frictionCoefficient;
    }

    resetPosition(): void {
        this.position.x = this.startPosition.x + getRandomNumberBetween(-2, 2);
        this.position.y = this.startPosition.y + getRandomNumberBetween(-2, 2);
        this.velocity.x = this.startVelocity.x;
        this.velocity.y = this.startVelocity.y;
    }
}
