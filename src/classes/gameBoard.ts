import { drawRectangle, drawCircle } from "../utils/drawingUtils.js";
import { Point } from "../types/types.js";

export class GameBoard {
    readonly canvas: HTMLCanvasElement;
    readonly ctx: CanvasRenderingContext2D;
    readonly width: number;
    readonly height: number;
    readonly center: Point;
    readonly lineWidth: number;
    readonly dotRadius: number;
    readonly goalPositions: Point[];
    readonly offSideLinePositions: Point[];
    readonly faceOffCirclePositions: Point[];
    readonly circleRadiusSmall: number;
    readonly circleRadiusLarge: number;
    readonly circleRadiusGoal: number;
    readonly iceColor: string;
    readonly redColor: string;
    readonly blueColor: string;

    constructor(
        canvasId: string, width: number, height: number, center: Point, lineWidth: number, dotRadius: number,
        goalPositions: Point[], offSideLinePositions: Point[], faceOffCirclePositions: Point[],
        circleRadiusSmall: number, circleRadiusLarge: number, circleRadiusGoal: number,
        iceColor: string, redColor: string, blueColor: string
    ) {
        this.canvas = document.getElementById(canvasId) as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
        this.width = width;
        this.height = height;
        this.canvas.width = width;
        this.canvas.height = height;
        this.center = center;
        this.lineWidth = lineWidth;
        this.dotRadius = dotRadius;
        this.goalPositions = goalPositions;
        this.offSideLinePositions = offSideLinePositions;
        this.faceOffCirclePositions = faceOffCirclePositions;
        this.circleRadiusSmall = circleRadiusSmall;
        this.circleRadiusLarge = circleRadiusLarge;
        this.circleRadiusGoal = circleRadiusGoal;
        this.iceColor = iceColor;
        this.redColor = redColor;
        this.blueColor = blueColor;
    }

    drawBackground(): void {
        drawRectangle(this.ctx, this.center, this.width, this.height, this.iceColor);
        drawRectangle(this.ctx, this.center, this.lineWidth, this.height, this.redColor);
        drawCircle(this.ctx, this.center, this.circleRadiusLarge, this.blueColor, this.lineWidth);

        for (const goalPosition of this.goalPositions) {
            drawCircle(this.ctx, goalPosition, this.circleRadiusGoal, this.blueColor, this.lineWidth);
        }
        for (const offSideLinePosition of this.offSideLinePositions) {
            drawRectangle(this.ctx, offSideLinePosition, this.lineWidth, this.height, this.blueColor);
        }
        for (const faceOffCirclePosition of this.faceOffCirclePositions) {
            drawCircle(this.ctx, faceOffCirclePosition, this.circleRadiusSmall, this.redColor, this.lineWidth);
            drawCircle(this.ctx, faceOffCirclePosition, this.dotRadius, this.redColor);
        }
    }
}
