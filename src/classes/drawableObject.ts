import { PhysicsBody } from "./physicsBody.js";
import { Point, Vector } from "../types/types.js";
import { MovementBox } from "./movementBox.js";
import { drawCircle } from "../utils/drawingUtils.js";

export abstract class DrawableObject extends PhysicsBody {
    readonly primaryColor: string;
    readonly accentColor: string;

    constructor(
        mass: number, radius: number, startPosition: Point, startVelocity: Vector,
        collisionCoefficient: number, frictionCoefficient: number, movementBox: MovementBox,
        primaryColor: string, accentColor: string
    ) {
        super(
            mass, radius, startPosition, startVelocity,
            collisionCoefficient, frictionCoefficient, movementBox
        );
        this.primaryColor = primaryColor;
        this.accentColor = accentColor;
    }

    drawObject(ctx: CanvasRenderingContext2D): void {
        const accentCircleRadius = this.radius / 5 * 4;
        const accentCircleWidth = this.radius / 8;
        drawCircle(ctx, this.position, this.radius, this.primaryColor);
        drawCircle(ctx, this.position, accentCircleRadius, this.accentColor, accentCircleWidth);
    }
}
