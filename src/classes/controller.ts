import { Vector } from "../types/types.js";

export class Controller {
    readonly targetVelocity: number;
    readonly moveLeftKey: string;
    readonly moveRightKey: string;
    readonly moveUpKey: string;
    readonly moveDownKey: string;

    constructor(targetVelocity: number, moveLeftKey: string, moveRightKey: string, moveUpKey: string, moveDownKey: string) {
        this.targetVelocity = targetVelocity;
        this.moveLeftKey = moveLeftKey;
        this.moveRightKey = moveRightKey;
        this.moveUpKey = moveUpKey;
        this.moveDownKey = moveDownKey;
    }

    private isMovingDiagonally(speed: Vector): boolean {
        return speed.x !== 0 && speed.y !== 0;
    }

    private handleKeyEvent(e: KeyboardEvent, velocity: Vector): void {
        const key = e.key;
        const isKeyDown = e.type === 'keydown';
        const targetVelocity = isKeyDown ? this.targetVelocity : 0;

        if (key === this.moveLeftKey) {
            velocity.x = -targetVelocity;
        } else if (key === this.moveRightKey) {
            velocity.x = targetVelocity;
        } else if (key === this.moveUpKey) {
            velocity.y = -targetVelocity;
        } else if (key === this.moveDownKey) {
            velocity.y = targetVelocity;
        }

        if (this.isMovingDiagonally(velocity)) {
            velocity.x /= Math.sqrt(2);
            velocity.y /= Math.sqrt(2);
        } else if ((key === this.moveLeftKey || key === this.moveRightKey) && velocity.y !== 0) {
            velocity.y = velocity.y > 0 ? this.targetVelocity : -this.targetVelocity;
        } else if ((key === this.moveUpKey || key === this.moveDownKey) && velocity.x !== 0) {
            velocity.x = velocity.x > 0 ? this.targetVelocity : -this.targetVelocity;
        }
    }

    addEventListeners(velocity: Vector): void {
        addEventListener('keydown', (e) => this.handleKeyEvent(e, velocity), false);
        addEventListener('keyup', (e) => this.handleKeyEvent(e, velocity), false);
    }
}
