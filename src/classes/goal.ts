import { Point } from "../types/types.js";
import { Box } from "./box.js";
import { Puck } from "./puck.js";

export class Goal extends Box {
    constructor(location: Point, width: number, height: number) {
        super(location, width, height)
    }

    isPuckInsideGoal(puck: Puck): boolean {
        const isWithinXBounds = this.isInsideLeftLimit(puck) && this.isInsideRightLimit(puck);
        const isWithinYBounds = this.isInsideTopLimit(puck) && this.isInsideBottomLimit(puck);
        return isWithinXBounds && isWithinYBounds;
    }
}
