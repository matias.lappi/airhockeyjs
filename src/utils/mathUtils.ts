import { Player } from "../classes/player.js";
import { Puck } from "../classes/puck.js";
import { GameObject, Point, Vector } from "../types/types.js";

function getDistanceBetweenTwoPoints(pointOne: Point, pointTwo: Point) {
    return Math.sqrt((pointTwo.x - pointOne.x) ** 2 + (pointTwo.y - pointOne.y) **2);
}

function getDistanceBetweenTwoObjects(objectOne: GameObject, objectTwo: GameObject) {
    const distanceBetweenCenters = getDistanceBetweenTwoPoints(objectOne.position, objectTwo.position);
    const distanceBetweenObjects = distanceBetweenCenters - (objectOne.radius + objectTwo.radius);
    return distanceBetweenObjects;
}

function getAngleBetweenTwoPoints(pointOne: Point, pointTwo: Point) {
    return Math.atan2(pointTwo.y - pointOne.y, pointTwo.x - pointOne.x);
}

function getVectorMagnitude(vector: Vector): number {
    return Math.sqrt(vector.x ** 2 + vector.y ** 2);
}

function getVectorAngle(vector: Vector): number {
    return Math.atan2(vector.y, vector.x);
}

function getVectorComponentAlong(vector: Vector, component: 'x' | 'y', targetAngle: number): number {
    const magnitude = getVectorMagnitude(vector);
    const angle = getVectorAngle(vector);
    const angleDifference = angle - targetAngle;
    if (component === 'x') {
        return magnitude * Math.cos(angleDifference);
    } else if (component === 'y') {
        return magnitude * Math.sin(angleDifference);
    } else {
        throw new Error("Invalid component. Must be 'x' or 'y'.");
    }
}

function calculateElasticCollision(m1: number, v1: number, m2: number, v2: number) {
    return (2 * m2 * v2 + v1 * (m1 - m2)) / (m1 + m2)
}

function calculateCollision(
    puck: Puck, otherObject: GameObject, angle: number, puckVelocityX: number, otherObjectVelocityX: number
): void {
    const puckFinalVelocityX = calculateElasticCollision(puck.mass, puckVelocityX, otherObject.mass, otherObjectVelocityX)
    const puckFinalVelocityY = getVectorComponentAlong(puck.velocity, 'y', angle);
    puck.velocity.x = Math.cos(angle) * puckFinalVelocityX + Math.cos(angle + Math.PI / 2) * puckFinalVelocityY;
    puck.velocity.y = Math.sin(angle) * puckFinalVelocityX + Math.sin(angle + Math.PI / 2) * puckFinalVelocityY;
}

export function handleObjectCollision(objectOne: GameObject, objectTwo: GameObject): void {
    if (objectOne instanceof Player && objectTwo instanceof Player) return;

    const distance = getDistanceBetweenTwoObjects(objectOne, objectTwo);

    if (distance > 0) return;

    const angle = getAngleBetweenTwoPoints(objectOne.position, objectTwo.position);

    const objectOneVelocityX = getVectorComponentAlong(objectOne.velocity, 'x', angle);
    const objectTwoVelocityX = getVectorComponentAlong(objectTwo.velocity, 'x', angle);

    if (!(objectOne instanceof Player)) {
        calculateCollision(objectOne, objectTwo, angle, objectOneVelocityX, objectTwoVelocityX);
    }
    if (!(objectTwo instanceof Player)) {
        calculateCollision(objectTwo, objectOne, angle, objectTwoVelocityX, objectOneVelocityX);
    }

    const overlap = -distance;
    if (overlap > 0) {
        objectOne.position.x -= overlap / 2 * Math.cos(angle);
        objectOne.position.y -= overlap / 2 * Math.sin(angle);
        objectTwo.position.x += overlap / 2 * Math.cos(angle);
        objectTwo.position.y += overlap / 2 * Math.sin(angle);
    }
}

export function getRandomNumberBetween(min: number, max: number): number {
    return Math.random() * (max - min) + min;
}
