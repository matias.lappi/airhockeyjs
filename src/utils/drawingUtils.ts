import { Point } from "../types/types";

export function drawRectangle(
    ctx: CanvasRenderingContext2D, position: Point, width: number, height: number, color: string
): void {
    ctx.fillStyle = color;
    ctx.fillRect(position.x - width / 2, position.y - height / 2, width, height);
}

export function drawCircle(
    ctx: CanvasRenderingContext2D, position: Point, radius: number, color: string, lineWidth?: number
): void {
    ctx.beginPath();
    if (lineWidth) {
        ctx.arc(position.x, position.y, radius - lineWidth / 2, 0, Math.PI * 2, true);
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;
        ctx.stroke();
    } else {
        ctx.arc(position.x, position.y, radius, 0, Math.PI * 2, true);
        ctx.fillStyle = color;
        ctx.fill();
    }
}
